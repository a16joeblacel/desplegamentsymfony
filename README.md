#Desplegament Symfony Joel Blanco


![Desplegament Apache](./symfony/Captura_Apache.png)



[Documentació utilitzada pel Desplegament ]( https://symfony.com/doc/current/setup/web_server_configuration.html)

##Instruccions per desplegar-ho amb un servidor Apache

- Si no tenim les llibreries a la carpeta vendor hem d'he descarregar-les amb la comanda: composer install --no-dev --optimize-autoloader
- Modificar variable d'entorn de "env" a "prod". Ho podem fer modificant el paràmetre "APP_ENV" en el fitxer .env
- Al fitxer composer.json hem d'eliminar totes les dependències de "require-dev" 
- Instal·lem una llibreria que ens ajudarà a desplegar-ho creant un fitxer .htaccess : composer require symfony/apache-pack
- Netejem la memoria cau del servidor amb la comanda: php bin/console cache:clear --env=prod --no-debug
- Hem d'habilitar el mòdul d'apache rewrite: a2enmod rewrite
- Per últim hem de modificar el virtualhost d'Apache per modificar el DocumentRoot perque apunti a la carpeta public de symfony


## Errors i Diferències

- El tutorial està correcte tot i que no explica alguns pasos que també s'han de dur a terme. 
- La part més complicada es donar-te compte de que les llibreries que s'utilitzen en desenvolupament s'han d'esborrar del composer per poder desplegar-ho correctament.

- No hi ha cap diferència entre instal·lar-ho a local i fer-ho al labs, en els dos casos s'utilitza Apache i la configuració és la mateixa tot i que no podem configurar Apache directament des del Virtualhost i ho hem de fer amb un fitxer htaccess.

* Al labs no es pot desplegar perque no esta el mòdul rewrite d'Apache instal·lat



